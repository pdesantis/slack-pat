# SlackPat

## Project Structure

### Core Data
- We make use of parent-child contexts
- The master context, `privateContenxt`, is of private queue concurrency type
- Its child, `mainContext`, is of main queue concurrency type
- We should generally avoid using `privateContext` directly, and instead use `mainContext`
- Most worker queues should be children of `mainContext`

### Network
###### NetworkService.swift
- A singleton class that handles all API interactions
- Contains a `NetworkURL` struct that has all API URLs
- Network responses are parsed on a serial operation queue and saved into a worker managed object context (child of `mainContext`)
- When the response contains multiple objects to be created or updated, the operation will do the following:
1. Gather all object IDs to fetch
2. Run a single batch fetch
3. Build a dictionary of IDs to objects
4. Update / create objects
5. Save


### Extensions
###### NSNotificationCenterExtension.swift
- Convenience methods posting / listening for notifications
- Constants for notification names

###### UIImageViewExtension.swift
- Convenience method for loading an image from a URL
- Allows cancelling the request
- Built-in protection against multiple image requests. New requests cancel old requests, and completing requests make sure they are the current request before setting the image. This comes in handy in table view cells.

## Notes
To test the phone & email links on the user profile page, test on a device, not on the simulator:
<http://stackoverflow.com/questions/25895634/email-composure-ios-8>

When tapping a phone link, constraint errors will appear in the console. This is another known issue:
<http://stackoverflow.com/questions/33345816/20-uiinputsetcontainerview-constraint-breaks-when-call-in-status-bar-gets-v>