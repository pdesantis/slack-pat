//
//  UserProfileTableViewCell.swift
//  SlackPat
//
//  Created by Patrick DeSantis on 3/30/16.
//  Copyright © 2016 Patrick DeSantis. All rights reserved.
//

import UIKit

class UserProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
}
