//
//  UserViewController.swift
//  SlackPat
//
//  Created by Patrick DeSantis on 3/29/16.
//  Copyright © 2016 Patrick DeSantis. All rights reserved.
//

import MessageUI
import UIKit

class UserViewController: UIViewController, MFMailComposeViewControllerDelegate, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileOverlayView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileImageTopSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var viewFilesButton: UIButton!

    var user: User!

    private enum ProfileField {
        case Email
        case Phone
        case Skype
        case TimeZone
        case Title
    }

    private var profileFields = [ProfileField]()

    private var timeZoneFormatter: NSDateFormatter?

    private let ProfileCellID = "ProfileCellID"
    private let defaultOverlayAlpha: CGFloat = 0.1

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = user.realName

        navigationController?.navigationBarHidden = true
        automaticallyAdjustsScrollViewInsets = false

        // Set up table view
        let cellNib = UINib(nibName: "UserProfileTableViewCell", bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: ProfileCellID)
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension

        // Set up data source
        if let _ = user.title {
            profileFields.append(.Title)
        }
        if let timeZoneOffset = user.timeZoneOffset {
            profileFields.append(.TimeZone)
            timeZoneFormatter = NSDateFormatter()
            timeZoneFormatter?.dateStyle = .NoStyle
            timeZoneFormatter?.timeStyle = .ShortStyle
            timeZoneFormatter?.timeZone = NSTimeZone(forSecondsFromGMT: timeZoneOffset.integerValue)
        }
        if let _ = user.email {
            profileFields.append(.Email)
        }
        if let _ = user.phone {
            profileFields.append(.Phone)
        }
        if let _ = user.skype {
            profileFields.append(.Skype)
        }

        nameLabel.text = user.realName
        userNameLabel.text = user.usernameForDisplay

        if let imageString = user.image_original, imageURL = NSURL(string: imageString) {
            profileImageView.sp_setImageWithURL(imageURL)
        }

        messageButton.layer.cornerRadius = 8
        messageButton.layer.borderColor = UIColor.lightGrayColor().CGColor
        messageButton.layer.borderWidth = 2

        viewFilesButton.layer.cornerRadius = messageButton.layer.cornerRadius
        viewFilesButton.layer.borderColor = messageButton.layer.borderColor
        viewFilesButton.layer.borderWidth = messageButton.layer.borderWidth
    }

    // MARK: - Actions
    @IBAction func backPressed(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }

    // MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profileFields.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(ProfileCellID, forIndexPath: indexPath) as! UserProfileTableViewCell
        let cellType = profileFields[indexPath.row]
        switch cellType {
        case .Email:
            cell.titleLabel.text = NSLocalizedString("Email", comment: "Email cell header")
            cell.descriptionLabel.text = user.email
            cell.descriptionLabel.textColor = Color.lightBlue
        case .Phone:
            cell.titleLabel.text = NSLocalizedString("Phone", comment: "Phone cell header")
            cell.descriptionLabel.text = user.phone
            if let phoneString = user.dialPhoneString, url = NSURL(string: phoneString)
                where UIApplication.sharedApplication().canOpenURL(url)
            {
                cell.descriptionLabel.textColor = Color.lightBlue
            } else {
                cell.descriptionLabel.textColor = UIColor.blackColor()
            }
        case .Skype:
            cell.titleLabel.text = NSLocalizedString("Skype", comment: "Skype cell header")
            cell.descriptionLabel.text = user.skype
            cell.descriptionLabel.textColor = UIColor.blackColor()
        case .TimeZone:
            if let timeZoneFormatter = timeZoneFormatter {
                cell.titleLabel.text = NSLocalizedString("Timezone", comment: "Timezone cell header")
                let timeString = timeZoneFormatter.stringFromDate(NSDate())
                cell.descriptionLabel.text = String(format: NSLocalizedString("%@ local time", comment: "Local time cell"), timeString)
                cell.descriptionLabel.textColor = UIColor.blackColor()
            }
        case .Title:
            cell.titleLabel.text = NSLocalizedString("Title", comment: "Title cell header")
            cell.descriptionLabel.text = user.title
            cell.descriptionLabel.textColor = UIColor.blackColor()
        }
        return cell
    }

    // MARK: - UIScrollViewDelegate
    func scrollViewDidScroll(scrollView: UIScrollView) {
        // Shift & strech the image view with scrolling
        let top = scrollView.contentInset.top + scrollView.contentOffset.y
        profileImageTopSpaceConstraint.constant = top

        // Change the opacity of the overlay with scrolling
        // Scale after quicker when scrolling up, so up gets darker faster
        let scaleFactor: CGFloat = (top < 0) ? 100 : 20
        let alpha = defaultOverlayAlpha + 0.1 * top / scaleFactor
        profileOverlayView.alpha = min(0.6, max(0, alpha))
    }

    // MARK: - UITableViewDelegate
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 67
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)

        let cellType = profileFields[indexPath.row]
        switch cellType {
        case .Email:
            sendEmail()
        case .Phone:
            makePhoneCall()
        default:
            break
        }
    }

    func makePhoneCall() {
        guard let phoneString = user.dialPhoneString, url = NSURL(string: phoneString) else { return }
        guard UIApplication.sharedApplication().canOpenURL(url) else { return }
        UIApplication.sharedApplication().openURL(url)
    }

    func sendEmail() {
        guard MFMailComposeViewController.canSendMail() else { return }
        guard let email = user.email else { return }
        let vc = MFMailComposeViewController()
        vc.setToRecipients([email])
        vc.mailComposeDelegate = self
        presentViewController(vc, animated: true, completion: nil)
    }

    // MARK: - MFMailComposeViewControllerDelegate
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }

}
