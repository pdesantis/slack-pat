//
//  UserListViewController.swift
//  SlackPat
//
//  Created by Patrick DeSantis on 3/29/16.
//  Copyright © 2016 Patrick DeSantis. All rights reserved.
//

import CoreData
import UIKit

class UserListViewController: UIViewController, NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!

    private var frc: NSFetchedResultsController!

    private let UserCellID = "UserCellID"

    override func viewDidLoad() {
        super.viewDidLoad()

        title = NSLocalizedString("Slack Team", comment: "Team vc header")

        // Set up table view
        let cellNib = UINib(nibName: "UserTableViewCell", bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: UserCellID)

        // Set up fetched results controller
        let request = NSFetchRequest(entityName: "User")
        request.predicate = NSPredicate(value: true)
        request.sortDescriptors = [NSSortDescriptor(key: "normalizedName", ascending: true)]
        frc = NSFetchedResultsController(fetchRequest: request,
                                         managedObjectContext: CoreDataStack.sharedInstance.mainObjectContext,
                                         sectionNameKeyPath: nil,
                                         cacheName: nil)
        frc.delegate = self
        let _ = try? frc.performFetch()

        // Don't show cell separators below our last row
        tableView.tableFooterView = UIView()
    }

    // MARK: - NSFetchedResultsControllerDelegate
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.reloadData()
    }

    // MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return frc.sections?[section].numberOfObjects ?? 0
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(UserCellID, forIndexPath: indexPath) as! UserTableViewCell
        let user = frc.objectAtIndexPath(indexPath) as! User
        cell.updateWithUser(user)
        return cell
    }

    // MARK: - UITableViewDelegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)

        guard let user = frc.objectAtIndexPath(indexPath) as? User else { return }
        let vc = UserViewController()
        vc.user = user
        navigationController?.pushViewController(vc, animated: true)
    }


}
