//
//  User+CoreDataProperties.swift
//  SlackPat
//
//  Created by Patrick DeSantis on 3/29/16.
//  Copyright © 2016 Patrick DeSantis. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension User {

    @NSManaged var email: String?
    @NSManaged var firstName: String?
    @NSManaged var hexColor: String?
    @NSManaged var image_24: String?
    @NSManaged var image_32: String?
    @NSManaged var image_48: String?
    @NSManaged var image_72: String?
    @NSManaged var image_192: String?
    @NSManaged var image_original: String?
    @NSManaged var lastName: String?
    @NSManaged var normalizedName: String?
    @NSManaged var phone: String?
    @NSManaged var realName: String?
    @NSManaged var skype: String?
    @NSManaged var teamID: String?
    @NSManaged var timeZoneOffset: NSNumber?
    @NSManaged var title: String?
    @NSManaged var userID: String
    @NSManaged var username: String?

}
