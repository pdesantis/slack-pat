//
//  CoreDataStack.swift
//  SlackPat
//
//  Created by Patrick DeSantis on 3/29/16.
//  Copyright © 2016 Patrick DeSantis. All rights reserved.
//

import CoreData
import Foundation

class CoreDataStack {

    let mainObjectContext: NSManagedObjectContext
    let privateObjectContext: NSManagedObjectContext

    private let managedObjectModel: NSManagedObjectModel
    private let persistentStoreCoordinator: NSPersistentStoreCoordinator

    static let sharedInstance = CoreDataStack()

    init() {

        let modelURL = NSBundle.mainBundle().URLForResource("SlackPat", withExtension: "momd")!
        managedObjectModel = NSManagedObjectModel(contentsOfURL: modelURL)!

        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
        let storeURL = documentsURL.URLByAppendingPathComponent("SlackPat.sqlite")

        persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)

        try! persistentStoreCoordinator.addPersistentStoreWithType(NSSQLiteStoreType,
                                                                   configuration: nil,
                                                                   URL: storeURL,
                                                                   options: [
                                                                    NSMigratePersistentStoresAutomaticallyOption: true,
                                                                    NSInferMappingModelAutomaticallyOption: true
            ])

        privateObjectContext = NSManagedObjectContext(concurrencyType: .PrivateQueueConcurrencyType)
        privateObjectContext.persistentStoreCoordinator = persistentStoreCoordinator

        mainObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        mainObjectContext.parentContext = privateObjectContext
    }

    func saveContexts() {
        mainObjectContext.performBlockAndWait {
            do {
                try self.mainObjectContext.save()
            } catch let error as NSError {
                print(error)
            }
        }

        privateObjectContext.performBlockAndWait {
            do {
                try self.privateObjectContext.save()
            } catch let error as NSError {
                print(error)
            }
        }
    }
}