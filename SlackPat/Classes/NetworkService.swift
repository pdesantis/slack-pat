//
//  NetworkService.swift
//  SlackPat
//
//  Created by Patrick DeSantis on 3/29/16.
//  Copyright © 2016 Patrick DeSantis. All rights reserved.
//

import CoreData
import Foundation

private struct NetworkURL {
    static let UserList = NSURL(string: "https://slack.com/api/users.list")!
}

private struct NetworkParameters {
    static let APIToken = "xoxp-4698769766-4698769768-18910479235-8fa82d53b2"
}

class NetworkService {

    static let sharedInstance = NetworkService()

    private let session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
    private let syncQueue = NSOperationQueue()

    init() {
        syncQueue.maxConcurrentOperationCount = 1
    }

    func fetchUserList() {
        let components = NSURLComponents(URL: NetworkURL.UserList, resolvingAgainstBaseURL: true)!
        let queryItem = NSURLQueryItem(name: "token", value: NetworkParameters.APIToken)
        components.queryItems = [queryItem]

        guard let url = components.URL else {
            print("Error resolving URL")
            return
        }

        let task = session.dataTaskWithURL(url) { (data, response, error) in
            // TODO: handle errors
            guard let data = data where error == nil else {
                print("Error downloading user list")
                return
            }

            guard let jsonOptional = try? NSJSONSerialization.JSONObjectWithData(data, options: []) as? NSDictionary, json = jsonOptional else {
                print("Couldn't parse JSON")
                return
            }

            guard let userJSONArray = json["members"] as? [NSDictionary] else {
                print("JSON didnt have proper 'members' key")
                return
            }

            let operation = ParseUserListOperation(userJSONArray: userJSONArray)
            self.syncQueue.addOperation(operation)
        }
        task.resume()
    }
    
}