//
//  Color.swift
//  SlackPat
//
//  Created by Patrick DeSantis on 3/30/16.
//  Copyright © 2016 Patrick DeSantis. All rights reserved.
//

import UIKit

struct Color {
    static let lightBlue = UIColor(red: 84.0/255.0, green: 201.0/255.0, blue: 249.0/255.0, alpha: 1)
}
