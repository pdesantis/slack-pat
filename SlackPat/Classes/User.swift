//
//  User.swift
//  SlackPat
//
//  Created by Patrick DeSantis on 3/29/16.
//  Copyright © 2016 Patrick DeSantis. All rights reserved.
//

import Foundation
import CoreData

class User: NSManagedObject {

    class func userFromJSON(jsonDictionary json: NSDictionary, withManagedObjectContext moc: NSManagedObjectContext) -> User? {
        guard let _ = json["id"] as? String else { return nil }
        guard let _ = json["name"] as? String else { return nil }
        if let deleted = json["deleted"] as? Bool {
            guard deleted == false else { return nil }
        }

        let user = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: moc) as! User
        user.updateWithJSON(jsonDictionary: json)
        return user
    }

    func updateWithJSON(jsonDictionary json: NSDictionary) {
        // Parse required fields, bailing if one is missing or invalid
        guard let userID = json["id"] as? String else { return }
        self.userID = userID

        guard let username = json["name"] as? String else { return }
        self.username = username

        // Parse optional top-level fields
        self.teamID = json["team_id"] as? String
        self.hexColor = json["color"] as? String

        if let timeZoneOffset = json["tz_offset"] as? Int {
            self.timeZoneOffset = NSNumber(integer: timeZoneOffset)
        } else {
            self.timeZoneOffset = nil
        }

        // Parse optional profile fields
        if let profileJSON = json["profile"] as? NSDictionary {
            self.firstName = profileJSON["first_name"] as? String
            self.lastName = profileJSON["last_name"] as? String
            self.realName = profileJSON["real_name"] as? String

            // Lowercase it here to make sorting case-insensitive
            let normalized = profileJSON["real_name_normalized"] as? String
            self.normalizedName = normalized?.lowercaseString

            self.title = profileJSON["title"] as? String

            self.skype = profileJSON["skype"] as? String
            self.phone = profileJSON["phone"] as? String
            self.image_24 = profileJSON["image_24"] as? String
            self.image_32 = profileJSON["image_32"] as? String
            self.image_48 = profileJSON["image_48"] as? String
            self.image_72 = profileJSON["image_72"] as? String
            self.image_192 = profileJSON["image_192"] as? String
            self.image_original = profileJSON["image_original"] as? String
            self.email = profileJSON["email"] as? String
        }
    }

    var usernameForDisplay: String? {
        guard let username = username else { return nil }
        return "@" + username
    }

    var dialPhoneString: String? {
        guard let phone = phone else { return nil }
        return "tel:" + phone
    }

}
