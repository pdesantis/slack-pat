//
//  ParseUserListOperation.swift
//  SlackPat
//
//  Created by Patrick DeSantis on 3/29/16.
//  Copyright © 2016 Patrick DeSantis. All rights reserved.
//

import CoreData
import Foundation

/**
 Parses a JSON dictionary of User objects and saves them to Core Data.
 This performs a single batch fetch of Users, then updates or creates as necessary.
 */
class ParseUserListOperation: NSOperation {

    private var userJSONArray: [NSDictionary]

    private let maxStride = 500

    init(userJSONArray: [NSDictionary]) {
        self.userJSONArray = userJSONArray
        super.init()
    }

    override func main() {
        let moc = NSManagedObjectContext(concurrencyType: .PrivateQueueConcurrencyType)
        moc.parentContext = CoreDataStack.sharedInstance.mainObjectContext
        moc.performBlockAndWait({

            let totalCount = self.userJSONArray.count

            for startIndex in 0.stride(to: totalCount, by: self.maxStride) {
                // Break the JSON array into smaller chunks so we're not fetching them all in memory at a time
                let endIndex = min(totalCount, startIndex + self.maxStride)
                let userJSONSlice = self.userJSONArray[startIndex..<endIndex]

                // Gather all user IDs
                let userIDs = userJSONSlice.flatMap { $0["id"] as? String }
                let uniqueUserIDs = Set(userIDs)

                // Fetch all users included in the JSON
                let request = NSFetchRequest(entityName: "User")
                request.predicate = NSPredicate(format: "userID in %@", uniqueUserIDs)
                let existingUsers = try! moc.executeFetchRequest(request) as! [User]

                // Build a dictionary of user IDs to user objects
                var userIDToUser = [String: User]()
                for user in existingUsers {
                    userIDToUser[user.userID] = user
                }

                // Create users
                for userJSON in userJSONSlice {
                    guard let userID = userJSON["id"] as? String else { continue }

                    // If this user is to be deleted
                    if let deleted = userJSON["deleted"] as? Bool where deleted {
                        // Delete the existing user if we have it
                        if let existingUser = userIDToUser[userID] {
                            moc.deleteObject(existingUser)
                        }
                    } else {
                        // Otherwise, create/update
                        // If we have an existing user, update!
                        if let existingUser = userIDToUser[userID] {
                            existingUser.updateWithJSON(jsonDictionary: userJSON)
                        } else {
                            // Otherwise, create a new user
                            let _ = User.userFromJSON(jsonDictionary: userJSON, withManagedObjectContext: moc)
                        }
                    }
                }
            }

            // Save
            let _ = try? moc.save()
            CoreDataStack.sharedInstance.saveContexts()

            // Notify listeners
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                NSNotificationCenter.postNotification(.UserListDownloadDidFinish)
            })
        })
    }
}
