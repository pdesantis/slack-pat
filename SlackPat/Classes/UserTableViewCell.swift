//
//  UserTableViewCell.swift
//  SlackPat
//
//  Created by Patrick DeSantis on 3/29/16.
//  Copyright © 2016 Patrick DeSantis. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        userImageView.layer.cornerRadius = 8
    }

    override func prepareForReuse() {
        userImageView.image = nil
        userImageView.sp_cancelImageRequest()
    }

    func updateWithUser(user: User) {
        if let imageString = user.image_192, imageURL = NSURL(string: imageString) {
            userImageView.sp_setImageWithURL(imageURL)
        }

        nameLabel.text = user.realName
        titleLabel.text = user.title
    }
    
}
