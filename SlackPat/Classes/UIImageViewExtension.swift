//
//  UIImageViewExtension.swift
//  SlackPat
//
//  Created by Patrick DeSantis on 3/30/16.
//  Copyright © 2016 Patrick DeSantis. All rights reserved.
//

import UIKit

extension UIImageView {

    private struct AssociatedKeys {
        static var ImageDataTaskKey = "ps_UIImageView.ImageDataTask"
    }

    var sp_imageDataTask: NSURLSessionDataTask? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.ImageDataTaskKey) as? NSURLSessionDataTask
        }
        set(downloader) {
            objc_setAssociatedObject(self, &AssociatedKeys.ImageDataTaskKey, downloader, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    private func isURLEqualToActiveURLRequest(url: NSURL) -> Bool {
        if let
            currentRequest = sp_imageDataTask?.originalRequest
            where currentRequest.URL?.absoluteString == url.absoluteString
        {
            return true
        }
        return false
    }

    public func sp_cancelImageRequest() {
        sp_imageDataTask?.cancel()
        sp_imageDataTask = nil
    }

    func sp_setImageWithURL(url: NSURL) {
        guard !isURLEqualToActiveURLRequest(url) else { return }

        sp_cancelImageRequest()

        let task = NSURLSession.sharedSession().dataTaskWithURL(url, completionHandler: { (data, response, error) in
            dispatch_async(dispatch_get_main_queue(), {

                guard self.isURLEqualToActiveURLRequest(url) else { return }

                if let data = data, image = UIImage(data: data) {
                    self.image = image
                }

                self.sp_imageDataTask = nil
            })
        })
        sp_imageDataTask = task

        task.resume()
    }
}