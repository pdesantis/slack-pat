//
//  NSNotificationCenterExtension.swift
//  SlackPat
//
//  Created by Patrick DeSantis on 3/29/16.
//  Copyright © 2016 Patrick DeSantis. All rights reserved.
//

import Foundation

enum Notification: String {
    case UserListDownloadDidFinish
}

extension NSNotificationCenter {

    static func addDefaultObserver(observer: AnyObject, selector: Selector, notification: Notification?, object: AnyObject? = nil) {
        NSNotificationCenter.defaultCenter().addObserver(observer, selector: selector, name: notification?.rawValue, object: object)
    }

    static func postNotification(notification: Notification, object: AnyObject? = nil, userInfo: [NSObject: AnyObject]? = nil) {
        NSNotificationCenter.defaultCenter().postNotificationName(notification.rawValue, object: object, userInfo: userInfo)
    }
    
}
