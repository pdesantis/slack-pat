//
//  SlackPatTests.swift
//  SlackPatTests
//
//  Created by Patrick DeSantis on 3/29/16.
//  Copyright © 2016 Patrick DeSantis. All rights reserved.
//

import CoreData
import XCTest
@testable import SlackPat

class UserTests: XCTestCase {

    let moc = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
    
    override func setUp() {
        super.setUp()

        let model = NSManagedObjectModel.mergedModelFromBundles([NSBundle(forClass: UserTests.self)])!
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: model)
        moc.persistentStoreCoordinator = coordinator
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func jsonDictionaryForFileNamed(fileName: String) -> NSDictionary {
        let path = NSBundle(forClass: UserTests.self).pathForResource("json/\(fileName)", ofType: "json")!
        let jsonData = NSData(contentsOfFile: path)!
        let json = try! NSJSONSerialization.JSONObjectWithData(jsonData, options: []) as! NSDictionary
        return json
    }

    func testParseUserFromJSON() {
        let userJSON = jsonDictionaryForFileNamed("user")
        let user = User.userFromJSON(jsonDictionary: userJSON, withManagedObjectContext: moc)
        XCTAssertNotNil(user, "failed to parse user from json")
    }

    func testNilWhenJSONMissingUserID() {
        let userJSON = jsonDictionaryForFileNamed("user-invalid-name")
        let user = User.userFromJSON(jsonDictionary: userJSON, withManagedObjectContext: moc)
        XCTAssertNil(user, "failed to parse user from json")
    }

    func testNilWhenJSONMissingUserName() {
        let userJSON = jsonDictionaryForFileNamed("user-invalid-id")
        let user = User.userFromJSON(jsonDictionary: userJSON, withManagedObjectContext: moc)
        XCTAssertNil(user, "failed to parse user from json")
    }
    
}
